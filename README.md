![JDriven Logo](http://blog.jdriven.com/logo.png)

# Spring Cloud Workshop


## Clone and run

```
git clone https://bitbucket.org/jdriven/spring-cloud-workshop
```


Import the project in your favorite IDE.

Run the `FrontendApplication` main class from the IDE or execute `mvn spring-boot:run`.

Go to http://localhost:8080/ and check out the app.

This is a monolithic Spring Boot application that calculates prime numbers.
It has a simple frontend (Thymeleaf) with a form that is posted to a MVC controller.
The controller calls a service bean `PrimeNumbersService` directly and gives back the result.


## Monolith no more!

First we will refactor from a monolithic application to a separate frontend and backing service application,
to be able to scale it better.

To make it easier, the `PrimeNumbersService` bean is already moved (or copied) to a separate module that
forms the new backing service: `prime-numbers-service`.

Open up the `PrimeNumbersServiceApplication` in `prime-numbers-service`.
Now, start the application. It will listen on http://localhost:8181/.

`TODO 1.1` Delete the `PrimeNumbersService` class from the frontend module. 
This service bean will not be called directly anymore.
> If we would compile the frontend module now, we would notice the compiler errors.
> There is currently no implementation of the `PrimeNumbersService` in the frontend application. We are going to fix this next.

We need to replace the direct service bean call with a REST call that connects to the backing service.
Our backing service endpoint, implemented in `PrimeNumbersController`, is a REST endpoint. (`@RestController`).
To call this endpoint from a client perspective we will use a [RestTemplate](http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#rest-resttemplate).

`TODO 1.2` In `PrimeServiceClient` add the following implementation:
```java
public PrimeNumbersResponse calculatePrimeNumbers(PrimeNumbersRequest primeNumbersRequest) {
    return restTemplate.postForObject(
        "http://localhost:8181/primenumbers",
        primeNumbersRequest, 
        PrimeNumbersResponse.class
    );
}
```


Since we replaced the old `PrimeNumbersService` bean with the new `PrimeServiceClient` we need to modify 
the `FrontendController` class to inject the new bean and call the method.
`TODO 1.3` In `FrontendController` replace the injection of `PrimeNumbersService` with `PrimeServiceClient`.
```java
private final PrimeServiceClient primeServiceClient;

public FrontendController(PrimeServiceClient primeServiceClient) {
    this.primeServiceClient = primeServiceClient;
}
```


> With Spring Boot 1.4.RELEASE (Spring Framework 4.3) it is no longer necessary to add `@Autowired` 
> to constructors. As long as you have a single constructor, Spring will implicitly consider it an autowire target.


`TODO 1.4` Change the call `primeNumbersService.primeNumbersService.calculatePrimeNumbers(...)`:
```java
PrimeNumbersResponse primeNumbersResponse = 
    primeServiceClient.calculatePrimeNumbers(
        new PrimeNumbersRequest(primenumbersform.getFrom(), primenumbersform.getTo())
    );
primeNumbers = primeNumbersResponse.getPrimeNumbers();
instanceId = primeNumbersResponse.getInstanceId();
```

After this, restart the frontend application and check that it still works.

> Notice that the reporting instance on the frontend webpage has changed to a prime-number-service instance.


## Service discovery

The `PrimeServiceClient` currently has a `RestTemplate` injected which calls our backing service 
with a hard coded URL.
This is not really robust since it will not work anymore when we run the application on a different platform.
Also when we scale out the backing service to multiple instances one hard coded URL will give us problems.

We will use service registration and service discovery with [Eureka](http://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/1.1.5.RELEASE/#spring-cloud-eureka)
and [Ribbon](http://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/1.1.5.RELEASE/#spring-cloud-ribbon)
 to connect the frontend to the `prime-number-service`.
The `PrimeNumbersServiceApplication` will register itself as `"PRIME-NUMBER-SERVICE"` with the Eureka server and the
FrontendController will use a Eureka client to discover this service and connect to the service
using Ribbon instead of using a hard coded URL.

Start the `EurekaServerApplication` from the eureka-server module to have the Eureka server running.
**We can keep this application running during the entire workshop!**

First we need to make sure that the `prime-numbers-service` is being registered as a service in Eureka.
`TODO 2.1` Modifying the `PrimeNumbersServiceApplication` class: 
```java
@SpringBootApplication
@EnableEurekaClient
public class PrimeNumbersServiceApplication {
    ...
}
```


`TODO 2.2` Modify the `application.yml` of the `prime-numbers-service`. 
Set the server.port property to 0 so every instance will listen on a random port. 
This will allow you to run multiple instances on the same machine.
```
server:
  port: 0
```


(Re)run the `PrimeNumbersServiceApplication` and check that it is being registered in Eureka.
> You can check Eureka at: http://localhost:8761/.

Now we are ready to modify the frontend to look for instances of the backing service in Eureka.

`TODO 2.3` Modify the `pom.xml` of the frontend module and add the dependencies for Eureka and Ribbon.
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-ribbon</artifactId>
</dependency>
```


`TODO 2.4` Modify the `FrontendApplication` class to enable the `@EnableDiscoveryClient` annotation.
```java
@SpringBootApplication
@EnableDiscoveryClient
public class FrontendApplication {
    ...
}
```


`TODO 2.5` Modify the `PrimeServiceClient` class and replace the hard coded URL.
```java
return restTemplate.postForObject(
    "http://prime-numbers-service/primenumbers",
    primeNumbersRequest, 
    PrimeNumbersResponse.class
);
```


(Re)run the `FrontendApplication` and it should discover the instance of the backing service
through Eureka and use it to calculate the numbers.


## Load balancing
Now start an additional instance of `PrimeNumbersServiceApplication` (so you have 2 running at the same time,
on different random ports). Both should be registered in Eureka.
Check http://localhost:8761/ to confirm you have 2 instances of `prime-numbers-service` running.

> Our `RestTemplate` bean definition in `PrimeNumbersServiceApplication` is annotated with `LoadBalanced`.
> This will configure it to use a `LoadBalancerClient`. 
> More info can be found [here](http://projects.spring.io/spring-cloud/spring-cloud.html#_spring_resttemplate_as_a_load_balancer_client)

If you submit the form in the frontend repeatedly you should see that the requests are load-balanced 
between the instances.

> Note: It can take up to 30 seconds for new instances to become active in the 'pool' and also 30 seconds
> for terminated instances to disappear from Eureka.


## Feign
We still have to specify a URL to reach the backing service. We can declare a interface to use Feign instead.

`TODO 4.1` Modify the `pom.xml` of the frontend module and add the dependency for feign.
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-feign</artifactId>
</dependency>
```


`TODO 4.2` Modify the `FrontendApplication` and add the `@EnableFeignClients`. 
This will scan for `@FeignClient` annotated interfaces.
```java
...
@EnableFeignClients
public class FrontendApplication {
    ...
}
```


To use Feign, we must define an interface to the backing service.
The interface is already partly prefabricated in the class `PrimeServiceFeignClient`. 
`TODO 4.3` Add the `@FeignClient` annotation on the interface:
```java
@FeignClient("prime-numbers-service")
public interface PrimeServiceFeignClient {
    ... 
}
```


In this annotation we also specified the service name. This is identical to the service name registered in Eureka. 

`TODO 4.4` Add the `@RequestMapping` annotation to the `PrimeServiceFeignClient` interface. 
This is to specify the path to the controller on the `prime-numbers-service` end: 
```java
@FeignClient("prime-numbers-service")
@RequestMapping("/primenumbers")
public interface PrimeServiceFeignClient {
    ... 
}
```


`TODO 4.5` Add a definition for the `calculatePrimeNumbers` method and add the correct annotations:
```java
@RequestMapping(method = RequestMethod.POST)
PrimeNumbersResponse calculatePrimeNumbers(@RequestBody PrimeNumbersRequest primeNumbersRequest);
```


> Based on this `@FeignClient` annotated interface, Spring will dynamically create a bean using Feign as implementation
> This means we can use our interface as dependency and inject it in other beans.

`TODO 4.6` Modify the `PrimeServiceClient` class and replace the `RestTemplate` with a call to this our new `PrimeServiceFeignClient` bean.
```java
private final PrimeServiceFeignClient primeServiceFeignClient;

public PrimeServiceClient(PrimeServiceFeignClient primeServiceFeignClient) {
    this.primeServiceFeignClient = primeServiceFeignClient;
}

public PrimeNumbersResponse calculatePrimeNumbers(PrimeNumbersRequest primeNumbersRequest) {
    return this.primeServiceFeignClient.calculatePrimeNumbers(primeNumbersRequest);
}
```


Rerun the FrontendApplication and check that it still works.


## Circuit breaker
Now that we have a separate backing service with instances that may scale up or down, we run the risk
of a cascade failure. If one of the backing services is down, then the calling service may also fail
and the service that is calling that service will fail etc.

To prevent our entire 'landscape' of services to break down, we can implement the [circuit breaker pattern](http://martinfowler.com/bliki/CircuitBreaker.html).
The same way a circuit breaker works in your house, it will prevent other parts of the system being
affected by a failing part.
In Spring Cloud we can use [Hystrix](http://projects.spring.io/spring-cloud/spring-cloud.html#_circuit_breaker_hystrix_clients) for this.

`TODO 5.1` Modify the `pom.xml` of the frontend module by adding the dependency for hystrix.
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-hystrix</artifactId>
</dependency>
```


`TODO 5.2` Add the `@EnableHystrix` annotation to the `FrontendApplication`. This enables the circuit breaker.
```java
...
@EnableHystrix
public class FrontendApplication {
    ...
}
```


`TODO 5.3` Modify the `PrimeServiceClient` class and add the `@HystrixCommand` annotation to the method
 we would like to be controlled by the circuit breaker.
```java
@HystrixCommand(fallbackMethod = "getDefaultPrimeNumbers")
public PrimeNumbersResponse calculatePrimeNumbers(PrimeNumbersRequest primeNumbersRequest) {
    ...
}
```


Whenever the circuit breaker is open (failure) it will use a fallback method. 
In this case we will have a method `getDefaultPrimeNumbers` to return a default response. 
This has already been declared as fallback method in our `@HystrixCommand` annotation.

`TODO 5.4` Create the fallback method implementation.
```java
public PrimeNumbersResponse getDefaultPrimeNumbers(PrimeNumbersRequest primeNumbersRequest) {
    return new PrimeNumbersResponse(Collections.emptyList(), "NONE");
}
```


Rerun the `FrontendApplication` and try the circuit breaker by stopping and starting some instances of
the prime-numbers-service backing service (note the delay).

While doing that, take a look at the Hystrix dashboard.
Start the `HystrixDashboardApplication` from the `hystrix-dashboard` module and browse to 
http://localhost:7979/. Enter the URL of the Hystrix stream: http://localhost:8080/hystrix.stream

> Note: To be able to see a request being passed to the `hystrix.stream` you have to make a request first.

## Config server
As a bonus we can externalise configuration. Just for fun we added a `maxResults` configuration
parameter that will limit the amount of prime numbers being returned in a single response by the 
`prime-numbers-service`. Take a look at `PrimeNumbersService` and see that the `primenumbers.maxResults`
is injected here with a default value of `9999`.

We want to configure it and set it to 5 by using externalized configuration.

First we need to run a config server that the backing services will connect to.
Our initial attempt will use a simple directory on the local filesystem as configuration repository.

`TODO 6.1` Create a directory `config-repo` in your home directory and create a file in this directory called 
`prime-numbers-service.yml` and add the following contents:
```
primenumbers:
  maxResults: 5
```


Start the `ConfigServiceApplication` from the config-server module.

Now we need to make sure that the `prime-numbers-service` actually reads the configuration from the config server.

`TODO 6.2` Modify the `pom.xml` of the `prime-numbers-service` module and add the dependency for spring config:
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-config</artifactId>
</dependency>
```


`TODO 6.3` Edit the `bootstrap.yml` of the `prime-numbers-service` module and add the following lines to enable the 
config client.
```
spring:
  application:
    name: prime-numbers-service
  cloud:
    config:
      enabled: true
      uri: http://localhost:8888
```

> `Bootstrap.yml` (or bootstrap.properties) is a configuration file like `application.yml` but for that bootstrap
> phase of an application context. More info can be found [here](http://projects.spring.io/spring-cloud/spring-cloud.html#_the_bootstrap_application_context)

(Re)run the `PrimeNumbersServiceApplication` (only have one instance for now, to simplify testing).
Test it by submitting the form in the frontend with a large range; it should only return 5 numbers at once.


## Git
Bonus bonus: the file system can be replaced by a Git repository. Create a Git repository somewhere on
the file system (or for example in Bitbucket/GitHub) with the same layout.
`TODO 7.1` Modify the `application.yml` in the config-server module to point to the Git repository.

```
spring:
  cloud:
    config:
      server:
        git:
          uri: file://${user.home}/config-repo
# Or use your own repo
```


Restart the `ConfigServerApplication` and the `PrimeNumbersServiceApplication` to force to reload the configuration.