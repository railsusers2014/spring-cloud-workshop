package com.jdriven.cloud.frontend.client;

import java.util.Collections;

import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class PrimeServiceClient {
	
    // TODO 4.6: Replace RestTemplate with PrimeServiceFeignClient.
    private final PrimeServiceFeignClient primeServiceFeignClient;

    public PrimeServiceClient(PrimeServiceFeignClient primeServiceFeignClient) {
        this.primeServiceFeignClient = primeServiceFeignClient;
    }

    // TODO 5.3: Enable HystrixCommand
    @HystrixCommand(fallbackMethod = "getDefaultPrimeNumbers")
    public PrimeNumbersResponse calculatePrimeNumbers(PrimeNumbersRequest primeNumbersRequest) {
    	return primeServiceFeignClient.calculatePrimeNumbers(primeNumbersRequest);
    }

    // TODO 5.4: Create fallback method
    public PrimeNumbersResponse getDefaultPrimeNumbers(PrimeNumbersRequest primeNumbersRequest) {
        return new PrimeNumbersResponse(Collections.emptyList(), "NONE");
    }
}
