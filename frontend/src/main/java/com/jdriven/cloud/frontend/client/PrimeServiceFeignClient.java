package com.jdriven.cloud.frontend.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// TODO 4.3: Add @FeignClient annotation
@FeignClient("prime-numbers-service")
// TODO 4.4: Add @RequestMapping annotation
@RequestMapping("/primenumbers")
public interface PrimeServiceFeignClient {

    // TODO 4.5: Define calculatePrimeNumbers method
	@RequestMapping(method = RequestMethod.POST)
	PrimeNumbersResponse calculatePrimeNumbers(@RequestBody PrimeNumbersRequest primeNumbersRequest);
}
