package com.jdriven.cloud.frontend.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;

import java.util.Arrays;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.jdriven.cloud.frontend.client.PrimeNumbersResponse;
import com.jdriven.cloud.frontend.client.PrimeServiceClient;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"eureka.client.enabled=false"})
public class FrontendControllerTest {

    @Autowired
    private FrontendController controller;

    // TODO Delete PrimeNumbersService in favor of the PrimeServiceClient
    @MockBean
    private PrimeServiceClient primeServiceClient;

    @Test
    public void testSubmitPrimenumbers() throws Exception {
        given(this.primeServiceClient.calculatePrimeNumbers(anyObject()))
                .willReturn(new PrimeNumbersResponse(Arrays.asList(1, 2, 3), "instance-1"));

        Model model = new ExtendedModelMap();
        PrimeNumbersForm form = new PrimeNumbersForm();
        form.setFrom(0);
        form.setTo(100);

        controller.submitPrimenumbers(form, model);

        Map<String, Object> modelMap = model.asMap();
        assertThat(modelMap.get("primenumbers"), equalTo(Arrays.asList(1, 2, 3)));
    }
}