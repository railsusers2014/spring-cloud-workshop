package com.jdriven.cloud.prime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PrimeNumbersServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrimeNumbersServiceApplication.class, args);
    }
}
