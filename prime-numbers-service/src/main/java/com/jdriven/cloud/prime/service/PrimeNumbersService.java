package com.jdriven.cloud.prime.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

@Service
public class PrimeNumbersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrimeNumbersService.class);

    @Value("${primenumbers.maxResults:9999}")
    private int maxResults;

    @PostConstruct
    void init() {
        LOGGER.info("Initialized with max results: {}", maxResults);
    }

    public List<Integer> calculatePrimeNumbers(int from, int to) {
        List<Integer> primeNumbers = new ArrayList<>();
        for (int i = from; primeNumbers.size() < maxResults && i <= to; i++) {
            int counter = getDivisorsCount(i);
            if (counter == 2) {
                primeNumbers.add(i);
            }
        }
        return primeNumbers;
    }

    private int getDivisorsCount(int i) {
        int counter = 0;
        for (int num = i; num >= 1; num--) {
            if (i % num == 0) {
                counter++;
            }
        }
        return counter;
    }
}
